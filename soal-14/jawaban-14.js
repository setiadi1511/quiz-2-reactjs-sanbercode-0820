var balok = (p,l,t) => {
    var rumus = p * l * t;
    return `Volume Balok dari panjang ${p} cm, lebar ${l} cm dan tinggi ${t} cm adalah ${rumus} cm3`;
}

var kubus = (s) => {
    var rumus = s * s * s;
    return `Volume Kubus dari sisi ${s} mm adalah ${rumus} mm3`;
}

console.log(balok(2,3,4));
console.log(kubus(2));