let warna = ["biru","merah","kuning","hijau"];

let dataBukuTambahan = {
    penulis: "jhon doe",
    tahunTerbit: 2020,
};
let buku = {
    nama : "programmer daras",
    jumlahHalaman: 172,
    warnaSampul: ["hitam"]
}
buku.warnaSampul = [...warna,...buku.warnaSampul];
const combined = {...dataBukuTambahan, ...buku};
console.log(combined)
