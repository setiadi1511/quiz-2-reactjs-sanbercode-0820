function filterBookPromise(colorful,amountOfPage) {
    return new Promise(function (resolve,reject) {
        var books = [
            {name:"shinchan",totalPage:50,isColorful:true},
            {name:"Kalkukus",totalPage:250,isColorful:false},
            {name:"doraemon",totalPage:40,isColorful:true},
            {name:"algoritma",totalPage:300,isColorful:false},
        ];
        if(amountOfPage>0){
            resolve(books.filter(x=>x.totalPage >= amountOfPage && x.isColorful == colorful));
        } else {
            var reason = new Error("maaf parameter salah");
            reject(reason);
        }
    })
}
module.exports = filterBookPromise;


function askBook(isColor,amount) {
    filterBookPromise(isColor,amount).then(function (fulfilled) {
            console.log(fulfilled);
        })
        .catch(function (error) {
            console.log(error.message);
        });
}
 
askBook(false,200); 
// console.log(filterBookPromise(false,30))