class BangunDatar {
    constructor(nama = "", method) {
        this._nama = nama;
        this._method = method;
    }
    get nama(){
        return this._nama;
    }
} 

class Lingkaran extends BangunDatar {
    constructor(nama) {
        super(nama);
        this._nama = nama;
    }    
    luas(r) {
        var rumus = 3.14 * r * r;
        return `Luas ${this._nama} dari jari ${r} adalah ${rumus}`;
    }
    keliling(r) {
        var rumus = 2 * 3.14 * r;
        return `Keliling ${this._nama} dari jari ${r} adalah ${rumus}`;
    }
}

class Persegi extends BangunDatar {
    constructor(name) {
        super(name);
        this._name = name;
    }
    luas(p,l) {
        var rumus = p * l;
        return `Luas ${this._nama} dari panjang ${p} dan lebar ${l} adalah ${rumus}`;
    }
    keliling(p,l) {
        var rumus = (2*p) + (2*l);
        return `Keliling ${this._nama} dari panjang ${p} dan lebar ${l} adalah ${rumus}`;
    }
}
 
var ling = new Lingkaran("Lingkaran")
console.log(ling.luas(3))
console.log(ling.keliling(3))

var segi = new Persegi("Persegi")
console.log(segi.luas(3,4))
console.log(segi.keliling(3,4))
